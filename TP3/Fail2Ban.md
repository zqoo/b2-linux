# Fail2Ban

- si quelqu'un se plante 3 fois de password pour une co SSH en moins de 1 minute, il est ban
-   vérifiez que ça fonctionne en vous faisant ban
-   afficher la ligne dans le firewall qui met en place le ban
-   lever le ban avec une commande liée à fail2ban

**Installation de Fail2ban**
```
zqo@db:~$ sudo apt install fail2ban
```

**Démarrage du service et démarrage dès qu'on lance la VM**
```
zqo@db:~$ sudo systemctl start fail2ban
zqo@db:~$ sudo systemctl enable fail2ban
```

## Config des jails

```
zqo@db:~$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
zqo@db:~$ sudo vim /etc/fail2ban/jail.local
#modif section sshd
[sshd] 
enabled = true 

bantime = 1h 
findtime = 1m
maxretry = 3
```

On restart

```
zqo@db:~$ sudo systemctl restart fail2ban
zqo@db:~$ sudo fail2ban-client status
Status
|- Number of jail:	1
`- Jail list:	sshd
```

## Test de ban

```
zqo@web:~$ ssh zqo@192.168.64.9
zqo@192.168.64.9's password: 
Permission denied, please try again.
zqo@192.168.64.9's password: 
Permission denied, please try again.
zqo@192.168.64.9's password: 
zqo@192.168.64.9: Permission denied (publickey,password).
```

On check
```
zqo@db:~$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed:	0
|  |- Total failed:	3
|  `- File list:	/var/log/auth.log
`- Actions
   |- Currently banned:	1
   |- Total banned:	1
   `- Banned IP list:	192.168.64.7
```

On deban avec la commande

```
zqo@db:~$ sudo fail2ban-client unban 192.168.64.7
1
```

Check
```
zqo@db:~$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed:	0
|  |- Total failed:	3
|  `- File list:	/var/log/auth.log
`- Actions
   |- Currently banned:	0
   |- Total banned:	1
   `- Banned IP list:
```

```
zqo@web:~$ ssh zqo@192.168.64.9
zqo@192.168.64.9's password: 

Last login: Sun Nov 20 18:15:46 2022 from 192.168.64.1
```