
# Reverse Proxy

  

## I. Setup

🖥️ **VM `proxy.tp3.linux`**

  

➜ **On utilisera NGINX comme reverse proxy**

  

- installer le paquet `nginx`

```

zqo@proxy:~$ sudo apt install nginx -y

```

  

- démarrer le service `nginx`

```

zqo@proxy:~$ sudo systemctl start nginx

zqo@proxy:~$ sudo systemctl enable nginx

```

- utiliser la commande `ss` pour repérer le port sur lequel NGINX écoute

```

zqo@proxy:~$ sudo ss -laputn | grep nginx

tcp LISTEN 0 511 0.0.0.0:80 0.0.0.0:* users:(("nginx",pid=1618,fd=6),("nginx",pid=1617,fd=6),("nginx",pid=1616,fd=6),("nginx",pid=1615,fd=6),("nginx",pid=1614,fd=6),("nginx",pid=1613,fd=6),("nginx",pid=1612,fd=6),("nginx",pid=1611,fd=6),("nginx",pid=1609,fd=6))

tcp LISTEN 0 511 [::]:80 [::]:* users:(("nginx",pid=1618,fd=7),("nginx",pid=1617,fd=7),("nginx",pid=1616,fd=7),("nginx",pid=1615,fd=7),("nginx",pid=1614,fd=7),("nginx",pid=1613,fd=7),("nginx",pid=1612,fd=7),("nginx",pid=1611,fd=7),("nginx",pid=1609,fd=7))

```

- ouvrir un port dans le firewall pour autoriser le trafic vers NGINX

```

zqo@proxy:~$ sudo firewall-cmd --add-port=80/tcp --permanent

success

zqo@proxy:~$ sudo firewall-cmd --reload

success

```

- utiliser une commande `ps -ef` pour déterminer sous quel utilisateur tourne NGINX

```

zqo@proxy:~$ ps -ef | grep nginx

root 1609 1 0 15:51 ? 00:00:00 nginx: master process /usr/sbin/nginx -g daemon on; master_process on;

www-data 1611 1609 0 15:51 ? 00:00:00 nginx: worker process

```

- vérifier que le page d'accueil NGINX est disponible en faisant une requête HTTP sur le port 80 de la machine

```

zqo@proxy:~$ curl localhost

<!DOCTYPE html>

<html>

<head>

<title>Welcome to nginx!</title>

<style>

```

  

➜ Configurer NGINX

 
-   nous ce qu'on veut, c'pas une page d'accueil moche, c'est que NGINX agisse comme un reverse proxy entre les clients et notre serveur Web
-   deux choses à faire :
    -   créer un fichier de configuration NGINX
        -   la conf est dans `/etc/nginx`
        -   procédez comme pour Apache : repérez les fichiers inclus par le fichier de conf principal, et créez votre fichier de conf en conséquence
       
```
zqo@proxy:~$ sudo vim /etc/nginx/conf.d/nginx.conf
zqo@proxy:~$ sudo cat /etc/nginx/conf.d/nginx.conf
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp2.linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying 
        proxy_pass http://192.168.64.7:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```
-   NextCloud est un peu exigeant, et il demande à être informé si on le met derrière un reverse proxy
       - y'a donc un fichier de conf NextCloud à modifier
       - c'est un fichier appelé `config.php`
```
zqo@web:~$ sudo vim /var/www/tp2_nextcloud/config/config.php
zqo@web:~$ sudo cat /var/www/tp2_nextcloud/config/config.php
<?php
$CONFIG = array (
  'instanceid' => 'ocrjbjale81g',
  'passwordsalt' => '5oK/SMNT3u2fH1SIRfzI6fimIG4uf1',
  'secret' => 'nuOh3osANvz2SdXXxODXBDWShbdY80/CiTCcdeI8sbJTEYFU',
  'trusted_domains' => 
  array (
	  0 => 'web.tp2.linux',
	  1 => '192.168.64.7',
	  2 => '192.168.64.10',
	  3 => 'proxy.tp3.linux',
  ),
  'trusted_proxies' => '192.168.64.10',
  'datadirectory' => '/var/www/tp2_nextcloud/data',
  'dbtype' => 'mysql',
  'version' => '25.0.0.15',
  'overwrite.cli.url' => 'http://web.tp2.linux',
  'dbname' => 'nextcloud',
  'dbhost' => '192.168.64.9:3306',
  'dbport' => '',
  'dbtableprefix' => 'oc_',
  'mysql.utf8mb4' => true,
  'dbuser' => 'nextcloud',
  'dbpassword' => 'zqo',
  'installed' => true,
);
```

➜ **Modifier votre fichier `hosts` de VOTRE PC**

-   pour que le service soit joignable avec le nom `web.tp2.linux`
-   c'est à dire que `web.tp2.linux` doit pointer vers l'IP de `proxy.tp3.linux`

```
theorivry@Theos-MacBook-Pro ~ % cat /etc/hosts | grep web
192.168.64.10	web.tp2.linux
```

# II. HTTPS

Le but de cette section est de permettre une connexion chiffrée lorsqu'un client se connecte. Avoir le ptit HTTPS :)

Le principe :

-   on génère une paire de clés sur le serveur `proxy.tp3.linux`
    -   une des deux clés sera la clé privée : elle restera sur le serveur et ne bougera jamais
    -   l'autre est la clé publique : elle sera stockée dans un fichier appelé _certificat_
        -   le _certificat_ est donné à chaque client qui se connecte au site
-   on ajuste la conf NGINX
    -   on lui indique le chemin vers le certificat et la clé privée afin qu'il puisse les utiliser pour chiffrer le trafic
    -   on lui demande d'écouter sur le port convetionnel pour HTTPS : 443 en TCP