# Monitoring


Dans ce sujet on va installer un outil plutôt clé en main pour mettre en place un monitoring simple de nos machines.

L'outil qu'on va utiliser est [Netdata](https://learn.netdata.cloud/docs/agent/packaging/installer/methods/kickstart).

### Installation
```
zqo@db:~$ sudo apt install netdata -y
```
```
zqo@db:~$ sudo systemctl start netdata
zqo@db:~$ sudo systemctl enable netdata
```

On recherche le port qu'utilise netdata et on l'autorise dans le firewall.
```
zqo@db:~$ sudo ss -ltpn | grep netdata
                                                                                                                                                                           
LISTEN 0      4096       127.0.0.1:19999      0.0.0.0:*    users:(("netdata",pid=2934,fd=4))  
```

```
zqo@db:~$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
zqo@db:~$ sudo firewall-cmd --reload
success
```

**On vérifie que ça fonctionne**

```
zqo@db:~$ curl 192.168.64.9:19999
<!DOCTYPE html>
<!-- SPDX-License-Identifier: GPL-3.0-or-later -->
<html lang="en">
<head>
    <title>netdata dashboard</title>
    <meta name="application-name" content="netdata">
```


## Config netdata pour reçevoir les alertes sur discord

Fichier conf pour les notifs
```
zqo@db:~$ sudo touch /etc/netdata/health.d/health_alarm_notify.conf
zqo@db:~$ sudo /etc/netdata/edit-config health_alarm_notify.conf
```
```
zqo@db:~$ sudo vim /etc/netdata/health.d/cpu.conf
zqo@db:~$ cd /etc/netdata/
zqo@db:~$ sudo ./edit-config health.d/cpu.conf
zqo@db:/etc/netdata$ cat health.d/cpu.conf 
template: cpu_usage
      on: system.cpu
  lookup: average -3spercentage foreach user,system
   units: %
   every: 10s
    warn: $this > 50
    crit: $this > 80
    info: CPU utilization of users or the systel itself
```

**Test**
```
zqo@db:~$ sudo apt install stress-ng -y
zqo@db:~$ sudo stress-ng -c 10 -l 60
```
Stress test réussi, on reçoit bien la notif discord.

**Autre test pour montrer que ça fonctionne**
```
zqo@db:~$ cd /usr/lib/netdata/plugins.d/
zqo@db:/usr/lib/netdata/plugins.d$ ./alarm-notify.sh test

# SENDING TEST WARNING ALARM TO ROLE: sysadmin
2022-11-23 19:06:21: alarm-notify.sh: INFO: sent discord notification for: db.tp2.linux test.chart.test_alarm is WARNING to 'monitoring'
# OK

# SENDING TEST CRITICAL ALARM TO ROLE: sysadmin
2022-11-23 19:06:21: alarm-notify.sh: INFO: sent discord notification for: db.tp2.linux test.chart.test_alarm is CRITICAL to 'monitoring'
# OK

# SENDING TEST CLEAR ALARM TO ROLE: sysadmin
2022-11-23 19:06:22: alarm-notify.sh: INFO: sent discord notification for: db.tp2.linux test.chart.test_alarm is CLEAR to 'monitoring'
# OK
```
